import moment from "moment";
import { KEY_EXECUTION_FLAG, KEY_NB_COFFEE, KEY_STATS_KFET } from "./constants";
import { client } from "./main";
import Config from "./utils/Config";
import MongoInstance from "./utils/MongoInstance";
import RedisCache from "./utils/RedisCache";

let cache: RedisCache;

async function processAdeConnector() {
  console.log("ade-connector");
  const redis_stats = await client.get(
    `${Config.getInstance().getAdeConnectorKeyPrefix()}${KEY_STATS_KFET}`
  );
  if (!redis_stats) {
    console.warn("no data for ade-connector");
    return;
  }

  const db = MongoInstance.getInstance().getDatabase();
  const stats = db.collection("ade-connector");
  const formatted_stats = JSON.parse(redis_stats).map((s: any) => ({
    _id: moment(s.date).format("YYYY-MM-DD"),
    ...s,
  }));
  for (let stat of formatted_stats) {
    try {
      await stats.insertOne(stat, {
        checkKeys: true,
      });
    } catch {
      continue;
    }
  }
  console.log("ade-connector stats updated");
}

async function processLydiaConnector() {
  console.log("lydia-connector");
  const redis_stats = await client.get(
    `${Config.getInstance().getLydiaConnectorKeyPrefix()}${KEY_NB_COFFEE}`
  );
  if (!redis_stats) {
    console.warn("no data for lydia-connector");
    return;
  }

  const db = MongoInstance.getInstance().getDatabase();

  let stats = db.collection("lydia-connector");
  if (
    !(await db.collections()).find(
      (c) => c.collectionName === "lydia-connector"
    )
  ) {
    stats = await db.createCollection("lydia-connector");
  }

  const current_date = moment().format("YYYY-MM-DD");

  const exists = await stats.findOne({
    _id: current_date as any,
  });

  if (exists !== null) {
    console.log("lydia-connector update");
    await stats.findOneAndReplace(
      {
        _id: current_date as any,
      },
      {
        products: {
          coffee: parseInt(redis_stats),
        },
      }
    );
  } else {
    console.log("lydia-connector insert");
    const payload = {
      _id: current_date,
      products: {
        coffee: parseInt(redis_stats),
      },
    } as any;
    try {
      await stats.insertOne(payload, {
        checkKeys: true,
      });
    } catch {}
  }

  console.log("lydia-connector stats updated");
}

/**
 * Main program
 */
async function run() {
  processAdeConnector();
  processLydiaConnector();
}

/**
 * Check if the main program should be executed
 * @returns True if it can be executed and false elsewhere
 */
async function check() {
  if (!cache) cache = RedisCache.getInstance(client);

  const execution_flag = !!(await cache.get(KEY_EXECUTION_FLAG));

  if (execution_flag) return;

  await run().finally(async () => {
    await cache.set(
      KEY_EXECUTION_FLAG,
      1,
      Config.getInstance().getCheckInterval()
    );
  });
}

export { check };

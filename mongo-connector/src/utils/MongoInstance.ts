import { MongoClient } from "mongodb";
import Config from "./Config";

class MongoInstance extends MongoClient {
  private static instance: MongoInstance;

  private constructor() {
    super(Config.getInstance().getMongoUrl());
  }

  public getDatabase() {
    return this.db(Config.getInstance().getMongoDB());
  }

  public static getInstance() {
    if (!MongoInstance.instance) {
      MongoInstance.instance = new MongoInstance();
    }
    return MongoInstance.instance;
  }
}

export default MongoInstance;

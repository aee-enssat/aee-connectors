import dotenv from "dotenv";

type TConfig = {
  mongo_url: string;
  mongo_db: string;
  ade_connector_key_prefix: string;
  lydia_connector_key_prefix: string;
  redis_host: string;
  redis_port: number;
  redis_key_prefix: string;
  check_interval: string;
};

class Config {
  private static instance: Config;

  private config: TConfig;

  private constructor() {
    dotenv.config();

    this.config = {
      mongo_url: process.env.MONGO_URL ?? "",
      mongo_db: process.env.MONGO_DB ?? "aee",
      ade_connector_key_prefix: process.env.ADE_CONNECTOR_KEY_PREFIX ?? "",
      lydia_connector_key_prefix: process.env.LYDIA_CONNECTOR_KEY_PREFIX ?? "",
      redis_host: process.env.REDIS_HOST ?? "127.0.0.1",
      redis_port: 6379,
      redis_key_prefix: process.env.REDIS_KEY_PREFIX ?? "",
      check_interval: process.env.CHECK_INTERVAL ?? "1day",
    };

    if (!isNaN(parseInt(process.env.REDIS_PORT ?? ""))) {
      this.config.redis_port = parseInt(process.env.REDIS_PORT!);
    }
  }

  public getMongoUrl() {
    return this.config.mongo_url;
  }

  public getMongoDB() {
    return this.config.mongo_db;
  }

  public getAdeConnectorKeyPrefix() {
    return this.config.ade_connector_key_prefix;
  }

  public getLydiaConnectorKeyPrefix() {
    return this.config.lydia_connector_key_prefix;
  }

  public getRedisHost() {
    return this.config.redis_host;
  }

  public getRedisPort() {
    return this.config.redis_port;
  }

  public getRedisKeyPrefix() {
    return this.config.redis_key_prefix;
  }

  public getCheckInterval() {
    return this.config.check_interval;
  }

  public static getInstance() {
    if (!Config.instance) {
      Config.instance = new Config();
    }
    return Config.instance;
  }
}

export default Config;

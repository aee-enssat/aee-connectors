# Youtube API Scrapper pour ENSSATV

Le but de ce projet est de pouvoir récupérer les videos de la chaîne d'enssatv via l'api google et de les mettres dans Redis.

## À propos de YOUTUBE_API_KEY
Vous pouvez ajouter l'API KEY dans le fichier `.env`.
Cette clé est nécessaire pour pouvoir utiliser l'API de youtube et récupérer les vidéos de la chaîne.

## Lancer le projet en local

Pour lancer en local faites `npm install` puis `npm run dev`.
import './config/initConfig';
import ApiSource from "./ApiSource";
import { redisClient, getAsync, setAsync } from './redis/redisClient';
import { checkInterval } from './config/initConfig';

const REDIS_KEY_PREFIX = "enssatv_video_";

async function fetchAndStoreVideos(): Promise<void> {
    try {
        console.log(`Fetching videos...`);
        const videos = await ApiSource.getVideos();
        const totalCount = videos.length;

        console.log(`${totalCount} video(s) found!`);
        let count = 1;

        for (const video of videos) {
            const videoKey = REDIS_KEY_PREFIX + video.id;

            // check if the video already exists in the database
            const hasVideo = await getAsync(videoKey);

            // if the video does not exist, add it to the database
            //console.log(`Checking videoKey: ${videoKey}, Exists: ${!!hasVideo}`);

            if (!hasVideo) {
                await setAsync(videoKey, JSON.stringify(video));
                console.log(`[ADD] Video ${video.id}: ${video.title} (${count}/${totalCount})`);
            } else {
                console.log(`[IGNORED] Video ${video.id}: ${video.title} - Already added - (${count}/${totalCount})`);
            }
            count++;
        }
    } catch (error) {
        console.error("Erreur lors de la récupération ou du stockage des vidéos:", error);
    }
}

// fetch and store videos every checkInterval
redisClient.on('ready', () => {
    console.log("Client Redis connecté et prêt.");
    setInterval(fetchAndStoreVideos, checkInterval);
});

import { createClient, RedisClientType } from 'redis';
import { promisify } from 'util';

export const redisClient: RedisClientType = createClient({
    url: `redis://${process.env.REDIS_HOST || '127.0.0.1'}:${process.env.REDIS_PORT || 6379}`,
    legacyMode: true
});

redisClient.on('error', (err: Error) => {
    console.error('Erreur Redis:', err);
});

redisClient.connect().catch(console.error);

export const getAsync = promisify(redisClient.get).bind(redisClient);
export const setAsync = promisify(redisClient.set).bind(redisClient);

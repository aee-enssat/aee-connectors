import { youtube_v3, youtube } from "@googleapis/youtube";

export type TVideoTag = "ENSSAT_RADIO" | "ENSSAT_TV" | "EMISSIONS_BONUS";

export interface IVideo {
    id: string;
    title: string;
    description: string;
    publishedAt: Date;
    thumbnailURL: string;
    tag: TVideoTag;
}

export default class ApiSource {

    private VIDEO_TAG_MAPPER: { [key: string]: TVideoTag } = {
        "ENSSATRADIO": "ENSSAT_RADIO",
        "ENSSATV": "ENSSAT_TV"
    };

    private CHANNEL_ID: string = process.env.YOUTUBE_CHANNEL_ID!;
    private UPLOADS_PLAYLIST_ID: string = "";

    private auth: string = process.env.YOUTUBE_API_KEY!;
    private api: youtube_v3.Youtube;

    constructor() {
        this.api = youtube({
            version: "v3",
            auth: this.auth
        });
    }

    public static async getVideos(): Promise<Array<IVideo>> {
        const apiSource = new this();

        // get the uploads playlist id
        await apiSource.fetchUploadsPlaylistId();

        // get the videos from the uploads playlist
        const apiVideos = await apiSource.fetchPlaylistVideos();
        const videos = await Promise.all(apiVideos.map(
            video => apiSource.formatVideo(video))
        );

        // delete null values
        return videos.filter(video => !!video);
    }

    private formatVideo(data: youtube_v3.Schema$PlaylistItem): IVideo | null {
        const snippet = data.snippet;
        if (!snippet) return null;

        return {
            id: data.snippet?.resourceId?.videoId!,
            title: snippet.title!,
            description: snippet.description!,
            publishedAt: new Date(snippet.publishedAt!),
            thumbnailURL: snippet.thumbnails?.medium?.url!,
            tag: this.getVideoTag(snippet.title!)
        };
    }

    private getVideoTag(title: string): TVideoTag {
        for (const [tag, value] of Object.entries(this.VIDEO_TAG_MAPPER)) {
            if (title.startsWith(`[${tag}]`)) {
                return value;
            }
        }
        return "EMISSIONS_BONUS"; // default tag
    }

    private async fetchUploadsPlaylistId(): Promise<void> {
        const response = await this.api.channels.list({
            part: ["contentDetails"],
            id: [this.CHANNEL_ID],
        });

        const data = response.data as youtube_v3.Schema$ChannelListResponse;
        const channel = data.items?.[0];

        if (!channel) {
            throw new Error("Impossible de récupérer les détails de la chaîne");
        }

        this.UPLOADS_PLAYLIST_ID = channel.contentDetails?.relatedPlaylists?.uploads!;
        if (!this.UPLOADS_PLAYLIST_ID) {
            throw new Error("Impossible de récupérer la playlist des uploads");
        }
    }

    private async fetchPlaylistVideos(): Promise<Array<youtube_v3.Schema$PlaylistItem>> {
        let pageToken: string | undefined;
        const items: youtube_v3.Schema$PlaylistItem[] = [];

        do {
            const response = await this.api.playlistItems.list({
                part: ["snippet"],
                playlistId: this.UPLOADS_PLAYLIST_ID,
                maxResults: 50, // 50 is the maximum value
                pageToken,
            });

            const data = response.data as youtube_v3.Schema$PlaylistItemListResponse;

            if (data.items) {
                items.push(...data.items);
            }
            pageToken = data.nextPageToken!;
        } while (pageToken);

        return items;
    }
}

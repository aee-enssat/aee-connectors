import * as dotenv from 'dotenv';

dotenv.config();

export const checkInterval = parseInt(process.env.CHECK_INTERVAL || '180000', 10);

if (isNaN(checkInterval)) {
    console.error('CHECK_INTERVAL doit être un nombre valide.');
    process.exit(1);
}
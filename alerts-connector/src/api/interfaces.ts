export interface ApiResponse {
    data: Event[];
}

export interface Event {
    id: number;
    attributes: {
        name: string;
        description: string;
        date: string;
        place: string;
        isPublic: boolean;
        cover?: {
            data?: {
                attributes?: {
                    url: string;
                }
            }
        };
    };
}

const fetch = require('node-fetch');
import { ApiResponse, Event } from './interfaces';

const apiURL: string | undefined = process.env.API_URL;

export async function fetchEvents(): Promise<Event[]> {
    try {
        const response = await fetch(apiURL!, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${process.env.STRAPI_TOKEN}`,
                'Content-Type': 'application/json'
            }
        });

        if (!response.ok) {
            throw new Error(`Erreur HTTP: ${response.status} - ${response.statusText}`);
        }

        const data = await response.json() as ApiResponse;
        return data.data;
    } catch (error) {
        console.error("Erreur lors de la récupération des événements:", error);
        return [];
    }
}

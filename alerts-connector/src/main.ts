import './config/initConfig';
import * as Api from './api/apiClient';
import * as Notifications from './notifications';
import { redisClient, getAsync, setAsync } from './redis/redisClient';
import { checkInterval } from './config/initConfig';

async function checkForNewEvents(): Promise<void> {
    try {
        const events = await Api.fetchEvents();
        const latestEvent = events.reduce((prev, current) => (prev.id > current.id) ? prev : current);

        const lastEventId = await getAsync('lastEventId');

        if (lastEventId === null || latestEvent.id > parseInt(lastEventId)) {
            await Notifications.Discord.sendDiscordAlert(latestEvent);
            await setAsync('lastEventId', latestEvent.id.toString());
            console.log(`Nouvel événement détecté et alerté : ${latestEvent.attributes.name}`);
        } else {
            console.log("Aucun nouvel événement détecté. En standby...");
        }
    } catch (error) {
        console.error("Erreur lors de la vérification des événements:", error);
    }
}

redisClient.on('ready', () => {
    console.log("Client Redis connecté et prêt.");
    setInterval(checkForNewEvents, checkInterval);
});

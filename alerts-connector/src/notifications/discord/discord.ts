import { formatDate } from '../../utils/dateFormatter';
import { Event } from '../../api/interfaces';
const fetch = require('node-fetch');

const webhookURL: string | undefined = process.env.WEBHOOK_URL;
const rolesToPing: string | undefined = process.env.WHO_PING;

export async function sendDiscordAlert(event: Event): Promise<void> {
    const publicStatus = event.attributes.isPublic ? "🌍 Ouvert au public" : "🔒 Réservé ENSSAT";
    let imageUrl = '';
    if (event.attributes.cover && event.attributes.cover.data && event.attributes.cover.data.attributes && event.attributes.cover.data.attributes.url) {
        imageUrl = `${process.env.BASE_URL}${event.attributes.cover.data.attributes.url}`;
    }

    let rolesPingText = '';
    if (rolesToPing) {
        rolesPingText = rolesToPing.split(',').map(role => `<@&${role.trim()}>`).join(' ');
        rolesPingText = `||${rolesPingText}||`; //to mark as hidden
    }

    const message = {
        content: rolesPingText,
        embeds: [
            {
                title: `🆕 Nouveau événement: ${event.attributes.name}`,
                description: `${event.attributes.description}\n\n📅 **Date**: Le ${formatDate(event.attributes.date)}\n📍 **Lieu**: ${event.attributes.place}\n${publicStatus}`,
                color: event.attributes.isPublic ? 0x57F287 : 0xED4245,
                image: {
                    url: imageUrl
                }
            }
        ]
    };

    await fetch(webhookURL!, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(message),
    });
}

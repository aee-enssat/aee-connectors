import { format } from 'date-fns-tz';
import { fr } from 'date-fns/locale';

/**
 * Formate une date selon le format et le fuseau horaire spécifiés.
 * @param date - La date à formater.
 * @param dateFormat - Le format de la date souhaité.
 * @param timezone - Le fuseau horaire à appliquer.
 * @returns La date formatée comme une chaîne de caractères.
 */
export function formatDate(date: Date | string, dateFormat: string = 'PPpp', timezone: string = 'Europe/Paris'): string {
    const dateObject = typeof date === 'string' ? new Date(date) : date;
    return format(dateObject, dateFormat, { locale: fr, timeZone: timezone });
}

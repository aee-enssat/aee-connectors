import { Redis } from "ioredis";
import Config from "./Config";

class RedisCache {
  private static instance: RedisCache;
  private redisInstance: Redis;

  private constructor(redisInstance: Redis) {
    this.redisInstance = redisInstance;
  }

  /**
   * Get a value stored in Redis
   * @param key Key to retrieve in Redis
   * @returns The value of the key
   */
  public async get(key: string) {
    return await this.redisInstance.get(
      `${Config.getInstance().getRedisKeyPrefix()}${key}`
    );
  }

  /**
   * Set a key with a value in Redis
   * @param key Key to set in Redis
   * @param value Value to store
   * @param ttl Time to live of the key (in seconds)
   */
  public async set(
    key: string,
    value: string | number | Buffer,
    ttl?: number | string
  ) {
    if (ttl) {
      await this.redisInstance.setex(
        `${Config.getInstance().getRedisKeyPrefix()}${key}`,
        ttl,
        value
      );
    } else {
      await this.redisInstance.set(
        `${Config.getInstance().getRedisKeyPrefix()}${key}`,
        value
      );
    }
  }

  /**
   * Get a key or set a value in Redis if the key does not exists
   * @param key Key to retrieve in Redis
   * @param ttl Time to live of the key (in seconds)
   * @param callback Function called if no key exists
   * @returns The value of the key
   */
  public async getOrSave(
    key: string,
    ttl?: number | string,
    callback?: () => string | number | Buffer
  ) {
    const value = await this.get(key);

    if (value) return value;

    if (!callback) return;

    const value_to_save = callback();
    if (ttl) {
      await this.set(key, value_to_save, ttl);
    } else {
      await this.set(key, value_to_save);
    }
    return value_to_save;
  }

  public static getInstance(redis?: Redis) {
    if (RedisCache.instance) return RedisCache.instance;
    if (!redis) throw new Error("An argument is required");
    RedisCache.instance = new RedisCache(redis);
    return RedisCache.instance;
  }
}

export default RedisCache;

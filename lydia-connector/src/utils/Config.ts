import dotenv from "dotenv";

type TConfig = {
  lydia_url: string;
  vendor_token: string;
  phones_list: string[];
  redis_host: string;
  redis_port: number;
  redis_key_prefix: string;
  check_interval: string;
};

class Config {
  private static instance: Config;

  private config: TConfig;

  private constructor() {
    dotenv.config();

    this.config = {
      lydia_url: process.env.LYDIA_URL ?? "",
      vendor_token: process.env.VENDOR_TOKEN ?? "",
      phones_list: process.env.PHONES_LIST?.split(",") ?? [],
      redis_host: process.env.REDIS_HOST ?? "127.0.0.1",
      redis_port: 6379,
      redis_key_prefix: process.env.REDIS_KEY_PREFIX ?? "",
      check_interval: process.env.CHECK_INTERVAL ?? "1day",
    };

    if (!isNaN(parseInt(process.env.REDIS_PORT ?? ""))) {
      this.config.redis_port = parseInt(process.env.REDIS_PORT!);
    }
  }

  public getLydiaURL() {
    return this.config.lydia_url;
  }

  public getVendorToken() {
    return this.config.vendor_token;
  }

  public getPhonesList() {
    return this.config.phones_list;
  }

  public getRedisHost() {
    return this.config.redis_host;
  }

  public getRedisPort() {
    return this.config.redis_port;
  }

  public getRedisKeyPrefix() {
    return this.config.redis_key_prefix;
  }

  public getCheckInterval() {
    return this.config.check_interval;
  }

  public static getInstance() {
    if (!Config.instance) {
      Config.instance = new Config();
    }
    return Config.instance;
  }
}

export default Config;

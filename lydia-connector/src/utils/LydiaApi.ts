import axios from "axios";
import Config from "./Config";

class LydiaApi {
  private static lydia_query = axios.create({
    baseURL: Config.getInstance().getLydiaURL(),
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
  });

  public static async transactionList(
    phone: string,
    startDate: string,
    endDate: string
  ) {
    const query = new URLSearchParams({
      vendor_token: Config.getInstance().getVendorToken(),
      phone: phone,
      startDate: startDate,
      endDate: endDate,
    });

    return await LydiaApi.lydia_query.post("/api/transaction/list", query);
  }
}

export default LydiaApi;

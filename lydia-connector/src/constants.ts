export const KEY_EXECUTION_FLAG = `EXECUTION_FLAG`;
export const KEY_TRANSACTIONS_LIST = `TRANSACTIONS_LIST`;
export const KEY_NB_COFFEE = `NUMBER_OF_COFFEE_SELLS`;
export const PRICE_COFFEE = "0.40";
export const PRICE_MACAREUX = "1.30";
import moment from "moment";
import LydiaApi from "./utils/LydiaApi";
import Config from "./utils/Config";
import { KEY_EXECUTION_FLAG, KEY_NB_COFFEE, PRICE_COFFEE, PRICE_MACAREUX } from "./constants";
import RedisCache from "./utils/RedisCache";
import { client } from "./main";

let cache: RedisCache;

/**
 * Main program
 */
async function run() {
  const tasks: Promise<number | undefined>[] = [];
  const start_date = moment().startOf("month").format("YYYY-MM-DD");
  const end_date = moment().endOf("month").format("YYYY-MM-DD");

  const saveNumberOfCoffee = async (
    phone: string
  ): Promise<number | undefined> => {
    console.log(`Processing ${phone}...`);
    const lydia_response = await LydiaApi.transactionList(
      phone,
      `${start_date} 00:00:00`,
      `${end_date} 00:00:00`
    );
    if (!lydia_response.data || lydia_response.data.status !== "ok") return;

    const data = lydia_response.data;
    const nb = (data.transactions as any[])
      .filter((t) => t.amount === PRICE_COFFEE || t.amount === PRICE_MACAREUX)
      .map((t) => parseFloat(t.amount))
      .reduce((acc, _) => (acc += 1), 0);
    console.log(`${nb} coffees sold this month by ${phone}`);
    return nb;
  };

  console.log("===PROCESS COFFEE===");
  Config.getInstance()
    .getPhonesList()
    .forEach(async (phone) => {
      console.log(`Add ${phone} to the task queue`);
      tasks.push(saveNumberOfCoffee(phone));
    });
  const nb_coffees = (await Promise.all(tasks)).reduce(
    (acc, coffee) => (acc! += coffee ?? 0),
    0
  );
  console.log(`Find a total of ${nb_coffees} coffees sold this month`);
  await cache.set(KEY_NB_COFFEE, nb_coffees ?? 0);
  console.log(`${KEY_NB_COFFEE}=${nb_coffees} saved to Redis`);
}

/**
 * Check if the main program should be executed
 * @returns True if it can be executed and false elsewhere
 */
async function check() {
  if (!cache) cache = RedisCache.getInstance(client);

  const execution_flag = !!(await cache.get(KEY_EXECUTION_FLAG));

  if (execution_flag) return;

  await run().finally(async () => {
    await cache.set(
      KEY_EXECUTION_FLAG,
      1,
      Config.getInstance().getCheckInterval()
    );
  });
}

export { check };

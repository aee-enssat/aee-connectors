import axios from "axios";
import { createHash } from "crypto";
import moment from "moment";
import { sync } from "node-ical";
import { data } from "./config.json";
import { KEY_EXECUTION_FLAG, KEY_STATS_KFET } from "./constants";
import { client } from "./main";
import Config from "./utils/Config";
import RedisCache from "./utils/RedisCache";

let cache: RedisCache;

function delay(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function fetchAllEdt() {
  console.log("Fetch all EDT start");

  for (let d of data) {
    const hash = createHash("md5").update(d.url).digest("hex");

    let retries = 1; //retries left
    let success = false;

    while (retries >= 0 && !success) {
      try {
        d.edt = (await cache.getOrSave(`EDT_${hash}`, 3600, async () => {
          console.log(`EDT ${d.name} not found in redis, fetching from ADE...`);
          return (await axios.get(d.url)).data;
        })) as string;

        //if we reach this point, the request was successful
        success = true;

        //add delay before next request
        await delay(1000);

      } catch (error: any) {
        console.error(`Error fetching EDT for ${d.name}:`, error.message);

        //else if we receive a 500 error, we retry the request
        if (error.response && error.response.status === 500) {
          retries--;
          console.log(`Received 500 error for ${d.name}, retrying... (${2 - retries} retries left)`);

          await delay(1000);

          //if we reach the max number of retries, we log the error and skip the request
          if (retries < 0) {
            console.log(`Max retries reached for ${d.name}, skipping...`);
          }
        } else {
          throw error;
        }
      }
    }
  }

  console.log("Fetch all EDT done");
}

async function simulate(date: moment.Moment) {
  const nb = {
    date: date.toISOString(),
    kfet_h: 0,
    kfet_c: 0,
  };

  data.forEach((v) => {
    const calendar = Object.values(sync.parseICS(v.edt))
      .filter((c: any) => {
        const startDate = moment(c.start);

        if (
          c.location &&
          startDate.isSame(date, "day") &&
          startDate.hour() >= 8 &&
          startDate.hour() <= 10
        ) {
          return true;
        }
      })
      .map((c: any) => ({
        place: (String(c.location ?? "?") as string).split("").slice(-1)[0],
        hour: moment(c.start).hour(),
      }));

    const salles = calendar.map((c) => c.place);
    const students = Math.floor(v.students / salles.length);
    calendar.forEach((c) => {
      if (["C", "D", "E", "F", "G", "N"].includes(c.place)) {
        nb.kfet_c += students;
      } else if (["H", "V"].includes(c.place)) {
        nb.kfet_h += students;
      }
    });
  });

  return nb;
}

/**
 * Main program
 */
async function run() {
  console.log("Run Fetch all EDT");
  await fetchAllEdt();
  console.log("Run Fetch all EDT done");

  const now = moment();
  const simulations = [];

  for (let i = 0; i < Config.getInstance().getSimulateDays(); i++) {
    let date = i === 0 ? now : now.add(1, "day");
    const simulation = await simulate(date);
    simulations.push(simulation);
    console.log(
      date.toLocaleString(),
      "KFET_C=",
      simulation.kfet_c,
      "KFET_H=",
      simulation.kfet_h
    );
  }

  await cache.set(KEY_STATS_KFET, JSON.stringify(simulations));
}

/**
 * Check if the main program should be executed
 * @returns True if it can be executed and false elsewhere
 */
async function check() {
  if (!cache) cache = RedisCache.getInstance(client);

  const execution_flag = !!(await cache.get(KEY_EXECUTION_FLAG));

  if (execution_flag) return;

  try {
    await run();
  } catch (error: any) {
    console.error('Error during run:', error.message);
  } finally {
    await cache.set(
      KEY_EXECUTION_FLAG,
      1,
      Config.getInstance().getCheckInterval()
    );
  }  
}


export { check, run };

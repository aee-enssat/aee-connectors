import { Redis } from "ioredis";
import Config from "./utils/Config";
import { check } from "./worker";

const client = new Redis({
  host: Config.getInstance().getRedisHost(),
  port: Config.getInstance().getRedisPort(),
});

client.on("connect", async () => {
  console.log("connected to redis");
  await check();
  setInterval(async () => await check(), 10000);
});

export { client };

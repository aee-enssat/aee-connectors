import dotenv from "dotenv";

type TConfig = {
  redis_host: string;
  redis_port: number;
  redis_key_prefix: string;
  check_interval: number;
  edt_expiration: number;
  simulate_days: number;
};

class Config {
  private static instance: Config;

  private config: TConfig;

  private constructor() {
    dotenv.config();

    this.config = {
      redis_host: process.env.REDIS_HOST ?? "127.0.0.1",
      redis_port: 6379,
      redis_key_prefix: process.env.REDIS_KEY_PREFIX ?? "",
      check_interval: 3600,
      edt_expiration: 28800,
      simulate_days: 2,
    };

    if (!isNaN(parseInt(process.env.REDIS_PORT ?? ""))) {
      this.config.redis_port = parseInt(process.env.REDIS_PORT!);
    }
    if (!isNaN(parseInt(process.env.CHECK_INTERVAL ?? ""))) {
      this.config.check_interval = parseInt(process.env.CHECK_INTERVAL!);
    }
    if (!isNaN(parseInt(process.env.EDT_EXPIRATION ?? ""))) {
      this.config.edt_expiration = parseInt(process.env.EDT_EXPIRATION!);
    }
    if (!isNaN(parseInt(process.env.SIMULATE_DAYS ?? ""))) {
      this.config.simulate_days = parseInt(process.env.SIMULATE_DAYS!);
    }
  }

  public getRedisHost() {
    return this.config.redis_host;
  }

  public getRedisPort() {
    return this.config.redis_port;
  }

  public getRedisKeyPrefix() {
    return this.config.redis_key_prefix;
  }

  public getCheckInterval() {
    return this.config.check_interval;
  }

  public getEdtExpiration() {
    return this.config.edt_expiration;
  }

  public getSimulateDays() {
    return this.config.simulate_days;
  }

  public static getInstance() {
    if (!Config.instance) {
      Config.instance = new Config();
    }
    return Config.instance;
  }
}

export default Config;

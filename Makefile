docker-ade-connector:
	cd ade-connector; \
	docker build -t ade-connector:latest .

docker-lydia-connector:
	cd lydia-connector; \
	docker build -t lydia-connector:latest .

docker-mongo-connector:
	cd lydia-connector; \
	docker build -t lydia-connector:latest .
	
docker-alerts-connector:
	cd alerts-connector; \
	docker build -t alerts-connector:latest .

docker-youtube-scrapper:
	cd youtube-scrapper; \
	docker build -t youtube-scrapper:latest .


docker: docker-ade-connector docker-lydia-connector docker-mongo-connector docker-alerts-connector docker-youtube-scrapper
